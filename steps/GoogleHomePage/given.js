import { Given } from "cucumber";
import googleHome from "../../pages/Home";
void
    Given("A web browser is at the Google home page", () => {
        googleHome.goTo();
    });