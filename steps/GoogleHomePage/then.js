import { Then } from "cucumber";
import googleResults from "../../pages/SearchResults";

Then(/^links related to "(.*)" are shown on the results page$/, keyword => {
  googleResults.verifyResults(keyword);
});