import { When } from "cucumber"
import googleHome from "../../pages/Home";

When(/^The user enters "(.*)" into the search bar$/, keyword => {
    googleHome.search(keyword);
});