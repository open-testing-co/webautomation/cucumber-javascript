import setValueAndEnter from "../support/actions/setValueAndEnter";
import goToURL from "../support/actions/goToURL";

class Home{
    get searchField(){
        return $(".gLFyf.gsfi");
    }

    get searchButton(){
        return $(".aajZCb .gN089b");
    }

    search(keyword){
        setValueAndEnter(keyword,this.searchField);
    }

    goTo(){
        goToURL("/");
    }
}

module.exports = new Home();